gedankenexperiment
- hack userdata

Was ist CORS?
- Für Cross-Origin Zugriff wird CORS benötigt
- Teil von HTTP
- lässt den Server entscheiden welche Hosts auf Inhalte zugreifen dürfen

OWASP TOP 10 Security Misconfiguration vulnerability
cross-site scripting (XSS) 


Wie funktioniert CORS
1. Anfrage an Webserver mit Origin-Header: http://localhost:8080/

2. Origin wird mit Access-Control-Allow-Origin Header verglichen

3. Bei übereinstimmung wird der Inhalt geliefert



Links:
https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS#Preflighted_requests
