const http = require('http');
const express = require("express");
const app = express();

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "http://localhost:8080");
  if (req.method === 'OPTIONS') {
    res.header("Access-Control-Allow-Methods", "GET, POST, DELETE");
    return res.status(200).json({});
  }
  next();
});


app.use("/user", (req, res) => {
    res.status(200).json({
        secret: 123
    });
});

// start server
const server = http.createServer(app);
server.listen(3000);